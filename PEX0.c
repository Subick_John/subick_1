/** PEX0.c
* ==============================================================
* Name: John Subick
* Section: M1/2
* Project: PEX 0 - ASCII Art
* Purpose: Understand how to use C standard library functions,
*          and user-defined functions.
* Documentation Statement:
* ==============================================================
* Instructions:
 * Complete and submit PEX0 as outlined in the writeup.
*/

#include<stdio.h>
#include<math.h>

void drawFlower();
void drawDog();
void drawArrow();

double triangleArea(double,double,double);

int main(void) {

//Exercise 2//
    int drawVar;

    printf("Input 1 for Flower,");
    printf("2 for Dog, and 3 for Arrow.\n");
    scanf("%d", &drawVar);

    if(drawVar == 1) {
        drawFlower();
    }

    if(drawVar == 2) {
        drawDog();
    }

    if(drawVar == 3) {
        drawArrow();
    }

//Excercise 3//

    double base;
    double vertical;
    double hypotenuse;
    double answer;

    printf("Input three sides of a right triangle: ");
    printf("Base, Vertical, Hypotenuse. \n");
    scanf("%lf %lf %lf", &base, &vertical, &hypotenuse);

    answer = triangleArea(base, vertical, hypotenuse);

    return 0;
}


//user_defined_functions//
void drawFlower(){
    printf("    _ _\n");
    printf("   (_\\_)\n");
    printf("  (__<_{}\n");
    printf("   (_/_)\n");
    printf("  |\\ |\n");
    printf("   \\\\| /|\n");
    printf("    \\|//\n");
    printf("     |/\n");
    printf(",.,.,|.,.,.\n");
    printf("^`^`^`^`^`^\n");
}

void drawDog(){
    printf("^..^      /\n");
    printf("/_/\\_____/\n");
    printf("   /\\   /\\\n");
    printf("  /  \\ /  \\\n");
}

void drawArrow(){
    printf("**\n");
    printf("**\n");
    printf("**\n");
    printf("**\n");
    printf("**\n");
    printf("****\n");
    printf("***\n");
    printf("**\n");
    printf("*\n");
}

double area;
double triangleArea(double base, double vertical, double hypotenuse){
    double half = (double)1/(double) 2;
    area = (base * vertical) *half;

    if((pow(hypotenuse, 2)) == (pow(base, 2) + (pow(vertical, 2)))) {
        printf("The area of a right triangle with a base side of %.0lf inches, ", base);
        printf("a height of %.0lf inches, and a hypotenuse of %.0lf inches is %.2lf square inches.", vertical, hypotenuse, area);
    }

    else{
        printf("Your inputs do not describe a right triangle!");
    }

    return 0.0;
}